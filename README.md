# Cloud Computing 6


## Name
Deep Learning Instrument Recognition

## Description
Project consists of three stages:
- Part 1: Model instrument recognition with Keras
- Part 2: Use AWS lambda to carry out the processing
- Part 3: Build front end with Flask

### Part 1 Notebooks folder
Use image processing techniques to predict instruments.  Extract frequency domain information and use this info to build up an image of the instrument over a short time segment.  Use these images to predict instrument type.
- `Instruments1.ipnyb` - exploration of the wav  files in time and frequency domains
- `Instruments2.ipynb` - use CNN model on the processed wav files to predict.  Convert model to tflite to reduce size of Docker image passed to AWS

### Part 2 AWS Lambda processing

`To do`


### Part 3 Flask Front end

`To do`




